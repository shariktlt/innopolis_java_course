package ru.innopolis.log;

import org.junit.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.junit.Assert.*;

public class MainTest {
    private static Logger logger = LoggerFactory.getLogger(MainTest.class);
    private Main main;

    @BeforeClass
    public static void beforeTest() {
        logger.info("=== Run test ===");

    }

    @Before
    public void before() {
        main = new Main();
    }

    @Test
    public void testIsHelloWorld() {
        assertTrue("Arg is not a HelloWorld", this.main.isHelloWorld("Helloworld"));
        assertFalse("Bad word", this.main.isHelloWorld("Something"));
    }

    @Test
    public void testIsString() {
        assertTrue("Is not a string", this.main.isString("Is string value") );
    }

    @After
    public void after() {
        logger.info("This is @After");
    }

    @AfterClass
    public static void afterClass() {
        logger.info("This is @AfteClass");
    }
}