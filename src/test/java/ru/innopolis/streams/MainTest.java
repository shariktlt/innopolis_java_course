package ru.innopolis.streams;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by innopolis on 13.10.16.
 */
public class MainTest {

    private Main main;
    private Mockery context;
    @Before
    public void before(){
        this.main = new Main();
        this.context = new JUnit4Mockery();
    }


    @Test
    public void testHandle(){
       /* StreamWriter sw = new StreamWriter() {
            @Override
            public Long write(String obj) {
                return new Long(5);
            }
        };*/
       final StreamWriter sw = context.mock(StreamWriter.class);
       context.checking(new Expectations(){{
           oneOf(sw).write("SomeText");
           will(returnValue(new Long(5)));
           oneOf(sw).write("HelloWorld");
           will(returnValue(new Long(10)));
       }});
        main.setStreamWriter(sw);

        assertEquals(new Long(5), main.handle("SomeText"));
        assertEquals(new Long(10), main.handle("HelloWorld"));
    }
}
