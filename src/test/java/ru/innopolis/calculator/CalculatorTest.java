package ru.innopolis.calculator;

import org.junit.Before;
import org.junit.Test;
//import static org.junit.Assert.*;
import static junit.framework.Assert.*;

/**
 * Created by innopolis on 13.10.16.
 */
public class CalculatorTest {
    Calculator calculator;
    @Before
    public void before(){
        calculator = new Calculator();
    }

    @Test
    public void testAdd(){
        assertEquals(3., calculator.add(1, 2));
        assertEquals(1., calculator.add(1,0));
        assertEquals("Test 1+0=0", 1., calculator.add(1,0));
    }

    @Test
    public void testSub(){
        assertEquals(5., calculator.sub(10,5));
        assertEquals(5., calculator.sub(-5, -10));
    }

    @Test
    public void testDiv(){
        assertEquals(4., calculator.div(12,3));
        assertEquals(1., calculator.div(4,4));
    }

    @Test
    public void testMult(){
        assertEquals(20., calculator.mult(2, 10));
    }

    @Test
    public void testSqrt(){
        assertEquals(11., calculator.sqrt(121));
    }
}
