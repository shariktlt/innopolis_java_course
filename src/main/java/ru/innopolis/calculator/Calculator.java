package ru.innopolis.calculator;

/**
 * Created by innopolis on 13.10.16.
 */
public class Calculator {
    public double add(double a, double b){
        return a+b;
    }

    public double sub(double a, double b){
        return a-b;
    }

    public double div(double a, double b){
        return a/b;
    }

    public double mult(double a, double b){
        return a*b;
    }

    public double sqrt(double a){
        return Math.sqrt(a);
    }
}
