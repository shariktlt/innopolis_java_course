package ru.innopolis.week1;

/**
 * Created by innopolis on 07.10.16.
 */
public class SevenTimeEventThread extends TimeEventThread {
    public SevenTimeEventThread(Timer timer) {
        super(timer);
    }

    @Override
    public void checkEvent(int seconds){
        if(seconds > 0 && seconds % 7 == 0){
            System.out.println("7 seconds passed");
        }
    }
}
