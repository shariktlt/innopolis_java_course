package ru.innopolis.week1;

/**
 * Created by innopolis on 07.10.16.
 */
public interface TimeEventInterface {
   void checkEvent(int seconds);
}
