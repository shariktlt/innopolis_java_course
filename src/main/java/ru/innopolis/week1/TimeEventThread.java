package ru.innopolis.week1;

/**
 * Created by innopolis on 07.10.16.
 */
public class TimeEventThread implements Runnable, TimeEventInterface {
    private Timer timer;

    public TimeEventThread(Timer timer) {
        this.timer = timer;
    }

    @Override
    public void run() {
        while(!Thread.currentThread().isInterrupted()){
            checkEvent(timer.getSeconds());
            try {
                synchronized (timer){
                    timer.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public void checkEvent(int seconds){
        if(seconds > 0 && seconds % 5 == 0){
            System.out.println("5 seconds passed");
        }
    }
}
