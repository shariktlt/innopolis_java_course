package ru.innopolis.week1;

/**
 * Created by innopolis on 07.10.16.
 */
public class ChronoThread implements Runnable {

    private Timer timer;

    public ChronoThread(Timer timer) {
        this.timer = timer;
    }


    @Override
    public void run() {
        while(!Thread.currentThread().isInterrupted()){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            int currentSecond = timer.incriment();
            System.out.println("Current seconds: " + currentSecond);
            synchronized (timer){
                timer.notifyAll();
            }
        }
    }
}
