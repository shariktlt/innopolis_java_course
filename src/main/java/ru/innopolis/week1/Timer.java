package ru.innopolis.week1;

/**
 * Created by innopolis on 07.10.16.
 */
public class Timer {
    private int seconds;

    public int getSeconds() {
        return seconds;
    }

    public int incriment() {
        return ++this.seconds;
    }
}
