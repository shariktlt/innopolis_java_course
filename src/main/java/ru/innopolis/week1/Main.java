package ru.innopolis.week1;

/**
 * Created by innopolis on 07.10.16.
 */
public class Main {
    public static void main(String[] args){
        Timer timer = new Timer();
        Thread chrono = new Thread(new ChronoThread(timer));
        Thread timeFiveSeconds = new Thread(new TimeEventThread(timer));
        Thread timeSevenSeconds = new Thread(new SevenTimeEventThread(timer));
        chrono.start();
        timeFiveSeconds.start();
        timeSevenSeconds.start();
    }
}
