package ru.innopolis.cars.entity;

/**
 * Created by innopolis on 05.10.16.
 */
public class Customer {
    private final String firstName;
    private final String lastName;
    private final int age;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Customer customer = (Customer) o;

        if (age != customer.age) return false;
        if (firstName != null ? !firstName.equals(customer.firstName) : customer.firstName != null) return false;
        return lastName != null ? lastName.equals(customer.lastName) : customer.lastName == null;

    }

    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + age;
        return result;
    }

    public Customer(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }


    /**
     *  This method returns first name
     * @return
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     *  This method set first name
     * @param firstName

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
     */
    /**
     *  This method returns last name
     * @return
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * This method set last name
     * @param lastName

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
     */
    /**
     * This method returns age
     * @return
     */
    public int getAge() {
        return age;
    }
/*
    public void setAge(int age) {
        this.age = age;
    }
    */
}
