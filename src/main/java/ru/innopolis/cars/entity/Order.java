package ru.innopolis.cars.entity;

/**
 * Created by innopolis on 05.10.16.
 */
public class Order {
    private final Customer customer;
/*
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        return customer != null ? customer.equals(order.customer) : order.customer == null;

    }

    @Override
    public int hashCode() {
        return customer != null ? customer.hashCode() : 0;
    }
*/
    public Order(Customer customer) {
        this.customer = customer;
    }

    /**
     * This method returns customer
     * @return
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     *  This method set customer

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
     * @param customer
     */
}
