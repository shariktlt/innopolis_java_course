package ru.innopolis.cars.entity;

import java.io.Serializable;

/**
 * Created by innopolis on 05.10.16.
 */
public class Car implements Serializable {
    private String model;
    private transient int year;
    public Car(String model) {
        this.model = model;
    }

    public Car(String model, int year) {
        this.model = model;
        this.year = year;
    }
/*
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        return model != null ? model.equals(car.model) : car.model == null;

    }

    @Override
    public int hashCode() {
        return model != null ? model.hashCode() : 0;
    }
*/
    /**
     *  This method returns the model
     * @return model
     */
    public String getModel() {
        return model;
    }

    /**
     *  This method set model
     * @param model
     */
    public void setModel(String model) {
        this.model = model;
    }
}
