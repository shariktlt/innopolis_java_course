package ru.innopolis.cars;

import ru.innopolis.cars.entity.Car;
import ru.innopolis.cars.entity.Order;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.*;

/**
 * Created by innopolis on 05.10.16.
 */
public class Main {
    private Map<Order, Car> orderRegistry = createOrderRegistry();
    private Set<Car> carSet = createCarSet();

    private Set<Car> createCarSet() {
        return new HashSet<>();
    }

    private Map<Order, Car> createOrderRegistry(){
        return new HashMap<>();
    }

    public static void main(String[] args){
        /*Main main = new Main();
        Car vesta1 = new Car("Lada Vesta"), vesta2 = new Car("Lada Vesta"), vesta3 = new Car("Lada Vesta");
        main.carSet.add(vesta1);
        main.carSet.add(vesta2);
        main.carSet.add(vesta3);*/
        /*Iterator<Car> carIterator = main.carSet.iterator();
        while(carIterator.hasNext()){
            Car car = carIterator.next();
            System.out.println(car+" "+car.getModel());
        }*/
        /*for(Car car: main.carSet){
            System.out.println(car);
        }*/
        /*try(DBResource dbResource = new DBResource();
            NetResource netResource = new NetResource()){
            System.out.println("success");
            throw new Exception("Oops");
        }catch(Exception e){
            e.printStackTrace();
        }*/

        try(FileOutputStream fos = new FileOutputStream("/tmp/car_serial"); ObjectOutputStream oos = new ObjectOutputStream(fos)){
            oos.writeObject(new Car("Lada vesta", 2016));
        }catch(Exception e){
            e.printStackTrace();
        }
         System.out.println("Success main");
    }
}
