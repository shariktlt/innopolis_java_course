package ru.innopolis.cars.resource;

/**
 * Created by innopolis on 05.10.16.
 */
public class DBResource implements AutoCloseable {

    @Override
    public void close() throws Exception {
        System.out.println("Closing DB resource");
        throw new RuntimeException("DB exception");
    }
}
