package ru.innopolis.cars.resource;

/**
 * Created by innopolis on 05.10.16.
 */
public class NetResource implements AutoCloseable {

    @Override
    public void close() throws Exception {
        System.out.println("Closing Net resource");
        throw new RuntimeException("Net exception");
    }
}
