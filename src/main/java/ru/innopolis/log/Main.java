package ru.innopolis.log;

import org.apache.log4j.MDC;
import org.apache.log4j.NDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by innopolis on 12.10.16.
 */
public class Main {
    private static Logger logger = LoggerFactory.getLogger(Main.class);
    private static final String HELLO_WORLD = "HelloWorld";
    public static void main(String[] args){

    }
    private static void doSome(){
        logger.info("Message from doSome");
    }

    public boolean isHelloWorld(String arg){
        boolean result = false;
        if(arg != null){
            result = HELLO_WORLD.toLowerCase().equals(arg.toLowerCase());
        }
        return result;
    }

    public boolean isString(Object arg){
        boolean result = false;
        if(arg != null){
            result = arg.getClass().equals(String.class);
        }
        return result;
    }
}
