package ru.innopolis.db_lession;

import ru.innopolis.db_lession.menu.Menu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Created by innopolis on 26.10.16.
 */
public class Cli {
    private String currentMode;
    private boolean run = true;
    private Menu menu;
    private Deque<Menu> menuTree;

    public Cli() {
        menuTree = new ArrayDeque<>();
    }

    public void init() throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        switchMenu("Main");
        try(InputStreamReader isr = new InputStreamReader(System.in); BufferedReader br = new BufferedReader(isr)){
           while(run){
               try {
                   menu.printInvite();
                   System.out.println(":back - back menu, :exit - exit from programm");
                   processInput(br.readLine());
               }catch (Exception e){
                   e.printStackTrace();
               }
           }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void processInput(String s) {
        if(s.startsWith(":")){
            proccessCommands(s.substring(1));
        }else{
            if(menu != null){
                menu.processInput(this, s);
            }
        }
    }

    private void proccessCommands(String s){
        switch(s){
            case "exit":
                exitCli();
                break;
            case "back":
                backMenu();
                break;
        }
    }

    public void switchMenu(String name) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        menu = (Menu) Class.forName("ru.innopolis.db_lession.menu."+name+"Menu").newInstance();
        menuTree.add(menu);
    }
    private void backMenu(){
        if( menuTree.size() > 0){
            menu = menuTree.pop();
        }else{
            exitCli();
        }
    }

    private void exitCli() {
        run = false;
        System.out.println("exit now");
    }
    public Menu getMenu(){
        return menu;
    }
}
