package ru.innopolis.db_lession;

import java.sql.*;

/**
 * Created by innopolis on 26.10.16.
 */
public class DB {
    private static DB dbHandler;
    private Connection connection;
    private DB() throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/course", "innopolis", "innopolis");
    }

    static{
        try {
            dbHandler = new DB();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static DB getInstance(){
        return dbHandler;
    }

    public Statement getStatement() throws SQLException {
        return connection.createStatement();
    }
    public PreparedStatement getPrepared(String sql) throws SQLException {
        return connection.prepareStatement(sql);
    }
}
