package ru.innopolis.db_lession.menu;

import ru.innopolis.db_lession.Cli;

/**
 * Created by innopolis on 26.10.16.
 */
public class FillListEditorMenu implements Menu {
    private int currentLessionId;

    public int getCurrentLessionId() {
        return currentLessionId;
    }

    public void setCurrentLessionId(int currentLessionId) {
        this.currentLessionId = currentLessionId;
    }

    @Override
    public void printInvite() {

    }

    @Override
    public void processInput(Cli cli, String input) {

    }

}
