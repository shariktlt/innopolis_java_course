package ru.innopolis.db_lession.menu;

import ru.innopolis.db_lession.Cli;
import ru.innopolis.db_lession.DB;

import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created by innopolis on 26.10.16.
 */
public class FillLessionListMenu implements Menu {
    @Override
    public void printInvite() {
        try(Statement st = DB.getInstance().getStatement()){
            ResultSet rs = st.executeQuery("SELECT id, topic, date FROM lessons");
            if(rs.wasNull()){
                System.out.println("No one lessions found");
            }else{
                System.out.println("Choose lessions to fill");
                while (rs.next()){
                    System.out.println(new StringBuilder()
                        .append(rs.getInt("id"))
                            .append(") ")
                            .append(rs.getString("topic"))
                            .append(" ")
                            .append(rs.getString("date"))
                    .toString());
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void processInput(Cli cli, String input) {
        try {
            cli.switchMenu("FillListEditor");
            ((FillListEditorMenu) cli.getMenu()).setCurrentLessionId(Integer.parseInt(input));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
