package ru.innopolis.db_lession.menu;

import ru.innopolis.db_lession.Cli;

/**
 * Created by innopolis on 26.10.16.
 */
public class MainMenu implements Menu {
    private String invite;
    {
        invite = new StringBuilder()
                .append("Select menu: \n")
                .append("1) Add user [add_user]\n")
                .append("2) Add lession [add_lession]\n")
                .append("3) Fill lession [fill_lession]\n")
                .toString();
    }
    @Override
    public void printInvite() {
        System.out.println(invite);
    }

    @Override
    public void processInput(Cli cli, String input) {
        switch(input){
            case "add_user":
            case "1":
                try {
                    cli.switchMenu("AddUser");
                } catch (Exception e){
                    e.printStackTrace();
                }
                break;
            case "add_lession":
            case "2":
                try {
                    cli.switchMenu("AddLession");
                } catch (Exception e){
                    e.printStackTrace();
                }
                break;
            case "fill_lession":
            case "3":
                try {
                    cli.switchMenu("FillLessionList");
                } catch (Exception e){
                    e.printStackTrace();
                }
                break;
        }
    }
}
