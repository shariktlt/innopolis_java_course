package ru.innopolis.db_lession.menu;

import ru.innopolis.db_lession.Cli;
import ru.innopolis.db_lession.DB;

import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

/**
 * Created by innopolis on 26.10.16.
 */
public class AddLessionMenu implements Menu{
    int currentStep = 0;
    String topic;
    Date date;
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    @Override
    public void printInvite() {
        if(currentStep == 0){
            System.out.println("Print topic");
        }else{
            System.out.println("Print date (YYYY-MM-DD)");
        }

    }

    @Override
    public void processInput(Cli cli, String input) {
        try {
            if (currentStep == 0) {
                topic = input;
            } else {

                date = format.parse(input);
                addLession();
            }
            currentStep = ++currentStep % 2;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void addLession() {
        try(PreparedStatement st = DB.getInstance().getPrepared("INSERT INTO lessons (topic, date) VALUES(?,?)")){
            st.setString(1, topic);
            st.setDate(2, new java.sql.Date(date.getTime()));
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
