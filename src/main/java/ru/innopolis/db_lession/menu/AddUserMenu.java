package ru.innopolis.db_lession.menu;

import ru.innopolis.db_lession.Cli;
import ru.innopolis.db_lession.DB;

import java.lang.reflect.Array;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by innopolis on 26.10.16.
 */
public class AddUserMenu implements Menu {
    @Override
    public void printInvite() {
        System.out.println("For add user print \"NAME LASTNAME\"");
    }

    @Override
    public void processInput(Cli cli, String input) {
        String[] args = input.split(" ");
        if(args.length == 2){
            try( PreparedStatement st = DB.getInstance().getPrepared("INSERT INTO users (name, last_name) VALUES (?,?)")){
                st.setString(1, args[0]);
                st.setString(2, args[1]);
                st.executeUpdate();
                st.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
