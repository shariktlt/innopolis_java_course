package ru.innopolis.db_lession.menu;

import ru.innopolis.db_lession.Cli;

/**
 * Created by innopolis on 26.10.16.
 */
public interface Menu {
    void printInvite();
    void processInput(Cli cli, String input);
}
