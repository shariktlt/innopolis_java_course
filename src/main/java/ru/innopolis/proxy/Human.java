package ru.innopolis.proxy;

/**
 * Created by innopolis on 10.10.16.
 */
public interface Human {
    int getAge();
    void setAge(int age);

    String getName();

    void setName(String name);
}
