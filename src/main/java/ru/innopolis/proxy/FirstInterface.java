package ru.innopolis.proxy;

/**
 * Created by innopolis on 10.10.16.
 */
public interface FirstInterface {
    int doSome();
}
