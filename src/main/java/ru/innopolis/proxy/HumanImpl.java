package ru.innopolis.proxy;

/**
 * Created by innopolis on 10.10.16.
 */
public class HumanImpl implements Human {
    private int age;
    private String name;

    @Override
    public int getAge() {
        return age;
    }

    @Override
    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
}
