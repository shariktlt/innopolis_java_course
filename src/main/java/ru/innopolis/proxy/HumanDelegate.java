package ru.innopolis.proxy;

/**
 * Created by innopolis on 10.10.16.
 */
public class HumanDelegate implements Human {
    private Human delegate = new HumanImpl();
    @Override
    public int getAge() {
        return delegate.getAge();
    }

    @Override
    public void setAge(int age) {
        delegate.setAge(age);
    }

    @Override
    public String getName() {
        return delegate.getName();
    }

    @Override
    public void setName(String name) {
        delegate.setName(name);
    }
}
