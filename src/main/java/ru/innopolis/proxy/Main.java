package ru.innopolis.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Created by innopolis on 10.10.16.
 */
public class Main {
    public static void main(String[] args){
        Human pureHuman = getHuman();
        Human humanProxy = createProxy(pureHuman);
        Human humanDelegate = new HumanDelegate();
        loop(pureHuman);
        loop(humanProxy);
        loop(humanDelegate);
    }

    static Human createProxy(Human arg){
        ClassLoader classLoader = Main.class.getClassLoader();
        InvocationHandler invocationHandler = new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                return method.invoke(arg, args);
            }
        };
        Human result = (Human) Proxy.newProxyInstance(classLoader,
                new Class[]{ Human.class, FirstInterface.class },
                invocationHandler);
        return result;
    }

    private static void loop(Human human){
        long ts = System.currentTimeMillis();
        for(int i=0; i < 100_000_000; i++){
            human.setAge((int)Math.random()*100);
        }
        System.out.println("Invoked for "+(System.currentTimeMillis() - ts) );
    }
    static Human getHuman(){
        return new HumanImpl();
    }
}
