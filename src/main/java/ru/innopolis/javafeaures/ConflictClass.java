package ru.innopolis.javafeaures;

import java.util.Comparator;

/**
 * Created by innopolis on 20.10.16.
 * Cause error then Iface1 and Iface2 contain same default methods
 */
public class ConflictClass implements Iface1, Iface2 {
    @Override
    public void some() {

    }
}
