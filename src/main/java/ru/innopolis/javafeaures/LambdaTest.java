package ru.innopolis.javafeaures;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by innopolis on 20.10.16.
 */
public class LambdaTest {
    List<String> list;
    int field = 0;
    public LambdaTest() {
        list = createList();
        list.add("one");
        list.add("two");
        list.add("three");
    }

    public void start(int arg){
        Test test = new Test();
        Collections.sort(list, test::doSome);
       // list.removeIf((a)->new String("two").equals(a));
        list.forEach((s)-> System.out.println(s));
    }

    private List createList() {
        return new ArrayList<>();
    }
}
