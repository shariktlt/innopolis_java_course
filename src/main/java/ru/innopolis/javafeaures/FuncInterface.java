package ru.innopolis.javafeaures;

import java.util.function.Predicate;

/**
 * Created by innopolis on 20.10.16.
 */
@FunctionalInterface
public interface FuncInterface {
    void doSome(); //single non-overridable method for lambda
    default void doElse(){

    }
}
