package ru.innopolis.streams;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by innopolis on 13.10.16.
 */
public class Main {
    private static Logger logger = LoggerFactory.getLogger(Main.class);
    private StreamWriter streamWriter = new WSStreamWrite();


    public static void main(String[] args){

    }

    public Long handle(String arg){
        Long key = this.streamWriter.write(arg);
        logger.info("Method handle arg = {}", arg);
        return key;
    }

    public StreamWriter getStreamWriter() {
        return streamWriter;
    }

    public void setStreamWriter(StreamWriter streamWriter) {
        this.streamWriter = streamWriter;
    }
}
