package ru.innopolis.streams;

/**
 * Created by innopolis on 13.10.16.
 */
public interface StreamWriter {
    Long write(String obj);
}
