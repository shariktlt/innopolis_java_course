package ru.innopolis.classloader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.innopolis.streams.*;

/**
 * Created by innopolis on 17.10.16.
 */
public class LoggableClassloader extends ClassLoader {
    private static Logger logger = LoggerFactory.getLogger(LoggableClassloader.class);
    public LoggableClassloader(ClassLoader parent) {
        super(parent);
    }


    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        logger.info("fond class name {}", name);
        /**
         *  1.Найти файл
         *  2. Подгрузить
         */
        return null;
    }
}
