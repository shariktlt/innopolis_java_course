package ru.innopolis.classloader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by innopolis on 17.10.16.
 */
public class Main {
    private static Logger logger = LoggerFactory.getLogger(ru.innopolis.classloader.Main.class);
    public static void main(String[] args){
        LoggableClassloader classLoader = new LoggableClassloader(getClassLoader());
        try {
            Class test = classLoader.loadClass("TestNotExistClassName");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static ClassLoader getClassLoader() {
        return Main.class.getClassLoader();
    }
}
