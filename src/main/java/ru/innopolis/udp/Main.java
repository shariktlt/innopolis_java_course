package ru.innopolis.udp;

import java.io.IOException;
import java.net.*;

/**
 * Created by innopolis on 19.10.16.
 */
public class Main {
    public static void main(String[] args) throws IOException {
        DatagramSocket datagramSocket = new DatagramSocket();
        byte[] mes = "Message from client".getBytes();
        DatagramPacket datagramPacket = new DatagramPacket(mes, mes.length, InetAddress.getByName("127.0.0.1"), 1213);
        datagramSocket.send(datagramPacket);
    }
}
