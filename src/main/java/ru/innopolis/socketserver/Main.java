package ru.innopolis.socketserver;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by innopolis on 19.10.16.
 */
public class Main {
    ServerSocket serverSocket;
    public static Logger logger =  LoggerFactory.getLogger(Main.class);
    boolean isRun = true;

    public Main(int port) throws IOException {
        this.serverSocket = new ServerSocket(port);
    }

    public static void main(String[] args){
        try {
            Main main = new Main(1212);
            main.loop();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loop() throws IOException {
        Socket socket = null;
        while(isRun && (socket = serverSocket.accept()) != null){
            processSocket(socket);
        }
    }

    private void processSocket(Socket socket) {
        logger.info("Open socket for "+socket.getInetAddress());
        Scanner scanner = null;
        try {
            scanner = new Scanner(socket.getInputStream());
            while(socket.isConnected() && !socket.isClosed() && scanner.hasNextLine()){
                logger.info("Message: {}", scanner.nextLine());
            }
        }catch (Exception e){
            logger.error("Socket exception",e);
        }finally {
            if(socket!= null){
                try {
                    logger.info("Close socket for {}", socket.getInetAddress());
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(scanner != null){
                scanner.close();
            }
        }
    }
}
