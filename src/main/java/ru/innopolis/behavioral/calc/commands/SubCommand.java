package ru.innopolis.behavioral.calc.commands;

import ru.innopolis.behavioral.calc.Calculator;
import ru.innopolis.behavioral.calc.common.Command;

/**
 * Created by innopolis on 08.11.16.
 */
public class SubCommand implements Command{

    @Override
    public String getCommandName() {
        return "sub";
    }

    @Override
    public String execute(String[] args, Calculator calc) {
        if(args.length < 2){
            throw new RuntimeException("need 2 or more elements");
        }
        Double res = Double.parseDouble(args[0]);
        for(int i = 1; i<args.length; ++i){
            res-=Double.parseDouble(args[i]);
        }
        return res.toString();
    }
}
