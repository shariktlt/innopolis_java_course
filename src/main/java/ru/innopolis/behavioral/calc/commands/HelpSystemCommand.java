package ru.innopolis.behavioral.calc.commands;

import ru.innopolis.behavioral.calc.Calculator;
import ru.innopolis.behavioral.calc.common.Command;

import java.util.Iterator;
import java.util.Map;

/**
 * Created by innopolis on 08.11.16.
 */
public class HelpSystemCommand implements Command {
    @Override
    public String getCommandName() {
        return "help";
    }

    @Override
    public String execute(String[] args, Calculator calc) {
        StringBuilder sb = new StringBuilder();
        sb.append("Command list:\n");
        Iterator it = calc.getCommands().entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            sb.append(((Command)pair.getValue()).getCommandName()+"\n");
        }
        return sb.toString();
    }
}
