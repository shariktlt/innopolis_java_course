package ru.innopolis.behavioral.calc.commands;

import ru.innopolis.behavioral.calc.Calculator;
import ru.innopolis.behavioral.calc.common.Command;

/**
 * Created by innopolis on 08.11.16.
 */
public class AddCommand implements Command {

    private final String name = "add";
    @Override
    public String getCommandName() {
        return name;
    }

    @Override
    public String execute(String[] args, Calculator calc) {
        double res = 0;
        for(String item: args){
            res +=  Double.parseDouble(item);
        }
        return new Double(res).toString();
    }
}
