package ru.innopolis.behavioral.calc.commands;

import ru.innopolis.behavioral.calc.Calculator;
import ru.innopolis.behavioral.calc.common.Command;

/**
 * Created by innopolis on 08.11.16.
 */
public class MultCommand implements Command {
    @Override
    public String getCommandName() {
        return "mult";
    }

    @Override
    public String execute(String[] args, Calculator calc) {
        Double res = 1d;
        for(String item: args){
            res*= Double.parseDouble(item);
        }
        return res.toString();
    }
}
