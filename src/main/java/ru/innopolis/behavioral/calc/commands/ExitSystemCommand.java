package ru.innopolis.behavioral.calc.commands;

import ru.innopolis.behavioral.calc.Calculator;
import ru.innopolis.behavioral.calc.common.Command;

/**
 * Created by innopolis on 08.11.16.
 */
public class ExitSystemCommand implements Command {
    @Override
    public String getCommandName() {
        return "exit";
    }

    @Override
    public String execute(String[] args, Calculator calc) {
        calc.setRun(false);
        return "Exit now";
    }
}
