package ru.innopolis.behavioral.calc;

import ru.innopolis.behavioral.calc.common.Command;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Created by innopolis on 08.11.16.
 */
public class Calculator {
    private Map<String,Command> commands = new HashMap<>();
    private final String[] systemCommandsStringList = {"HelpSystemCommand","ExitSystemCommand"};
    private boolean isRun = true;
    public void setCommands(String[] args){
        for(String item: args){
           safeLoadCommand("commands."+item);
        }
    }

    private void safeLoadCommand(String name){
        try{
            Command command =  loadModule(name, Command.class);
            commands.put(command.getCommandName(), command);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void configure(Config config){
        this.setCommands(config.getCommands());
        this.setCommands(systemCommandsStringList);
    }

    public void init(Config config){
        configure(config);
        try(InputStreamReader is = new InputStreamReader(System.in); BufferedReader br = new BufferedReader(is)){
            String line = null;
            while(isRun){
                System.out.print("calc> ");
                line = br.readLine();
                try {
                    if (!line.isEmpty()) {
                        String[] args = line.split(" ");
                        runCommand(args);
                    }
                }catch (Exception e){
                    System.out.println("Error: "+e.getMessage());
                  //  e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void runCommand(String[] args) {
        if(args.length==0){
            return;
        }
        String cmd = args[0];
        String[] arg = Arrays.copyOfRange(args, 1, args.length);
        String res = executeCommand(cmd, arg);
        System.out.println(res);
        registerCommandUsage(cmd, arg, res);
    }

    private void registerCommandUsage(String cmd, String[] args, String res) {

    }

    private String executeCommand(String cmd, String[] args) {
        Command command = commands.get(cmd);
        if (command == null) {
            throw new RuntimeException("command " + cmd + " not found");
        }
        return command.execute(args, this);
    }

    private <T> T loadModule(String name, final Class<T> type ){
        try{
            return type.cast(Class.forName("ru.innopolis.behavioral.calc."+name).newInstance());
        } catch(final InstantiationException e){
            throw new IllegalStateException(e);
        } catch(final IllegalAccessException e){
            throw new IllegalStateException(e);
        } catch(final ClassNotFoundException e){
            throw new IllegalStateException(e);
        }
    }

    public Map<String, Command> getCommands() {
        return commands;
    }

    public void setRun(boolean run) {
        isRun = run;
    }
}
