package ru.innopolis.behavioral.calc;

/**
 * Created by innopolis on 08.11.16.
 */
public class Main {
    public static void main(String[] args) {
        Config config = new Config();
        Calculator calculator = new Calculator();
        calculator.init(config);
    }
}
