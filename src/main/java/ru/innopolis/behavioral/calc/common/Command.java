package ru.innopolis.behavioral.calc.common;

import ru.innopolis.behavioral.calc.Calculator;

/**
 * Created by innopolis on 08.11.16.
 */
public interface Command {
    String getCommandName();
    String execute(String[] args, Calculator calc);
}
