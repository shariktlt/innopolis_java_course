package ru.innopolis.reflect;


import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Type;

/**
 * Created by innopolis on 10.10.16.
 */
public class Main {
    public static void main(String[] args) throws Exception{
        Class<TypeForTest> clazz = TypeForTest.class;

        System.out.println(clazz.getAnnotations()[0]);
        for(Constructor constructor: clazz.getConstructors()){
            System.out.println(constructor.getName());
            constructor.setAccessible(false);
        }
        TypeForTest typeForTest = new TypeForTest();
        Field[] fields = clazz.getDeclaredFields();
        for(Field field: fields){
            field.setAccessible(true);
            field.set(typeForTest, 6);
            System.out.println(field.getName() + " " + field.get(typeForTest));

        }
        System.out.println("exit main");
    }
}
