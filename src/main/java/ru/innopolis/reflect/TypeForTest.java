package ru.innopolis.reflect;

/**
 * Created by innopolis on 10.10.16.
 */
@MyAnnotation
public class TypeForTest {
    @MyAnnotation
    private final int privateField = 5;
    protected int protectedField;
    public int publicField;

}
