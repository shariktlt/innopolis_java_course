package ru.innopolis.reflect;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by innopolis on 10.10.16.
 */

@Retention(RetentionPolicy.RUNTIME)
public @interface MyAnnotation {
    int value = 0;

}
