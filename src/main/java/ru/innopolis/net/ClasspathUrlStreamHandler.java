package ru.innopolis.net;


import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

/**
 * Created by innopolis on 19.10.16.
 */
public class ClasspathUrlStreamHandler  extends URLStreamHandler {
    private final ClassLoader classLoader;

    public ClasspathUrlStreamHandler(ClassLoader classloader){
        this.classLoader = classloader;
    }

    @Override
    protected URLConnection openConnection(URL url) throws IOException {
        String path = url.getPath();
        final URL resource = classLoader.getResource(path);
        return resource.openConnection();
    }

}
