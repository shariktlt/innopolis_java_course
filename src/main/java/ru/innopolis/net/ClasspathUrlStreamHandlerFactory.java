package ru.innopolis.net;

import java.net.URLStreamHandler;
import java.net.URLStreamHandlerFactory;

/**
 * Created by innopolis on 19.10.16.
 */
public class ClasspathUrlStreamHandlerFactory implements URLStreamHandlerFactory {
    private static String PREFIX = "sun.net.www.protocol";
    @Override
    public URLStreamHandler createURLStreamHandler(String s) {
        URLStreamHandler streamHandler = null;
        if("classpath".equals(s)){
            streamHandler = new ClasspathUrlStreamHandler(ClasspathUrlStreamHandlerFactory.class.getClassLoader());
        }else{
            streamHandler = getDefault(s);
        }
        return streamHandler;
    }

    private URLStreamHandler getDefault(String var1){
        String var2 = PREFIX + "." + var1 + ".Handler";
        try {
            Class var3 = Class.forName(var2);
            return (URLStreamHandler)var3.newInstance();
        } catch (ReflectiveOperationException var4) {
            throw new InternalError("could not load " + var1 + "system protocol handler", var4);
        }
    }
}
