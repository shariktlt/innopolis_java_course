package ru.innopolis.net;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandlerFactory;
import java.util.Scanner;
import java.util.stream.Stream;

/**
 * Created by innopolis on 19.10.16.
 */
public class Main {
    public static void main(String[] args){

        URL.setURLStreamHandlerFactory( new ClasspathUrlStreamHandlerFactory());
        Scanner scanner = null;
        try {
            URL url = new URL("classpath:./test.file");
            scanner = new Scanner(url.openStream());
            while(scanner.hasNext()){
                System.out.println(scanner.next());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if(scanner != null){
                scanner.close();
            }
        }
    }
}
