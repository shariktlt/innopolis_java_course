package ru.innopolis.patterns.builder;

/**
 * Created by innopolis on 25.10.16.
 */
public class Main {
    public static void main(String[] args) {
        //ComputerBuilderManager bm = new ComputerBuilderManager();
        //bm.setComputerBuilder(new IntelComputerBuilder());
        Computer comp = new Computer.Builder()
                .withMotherBoard("Motherboard")
                .withCpu("CPU")
                .withHdd("HDD")
                .withOs("OS").build();


        System.out.println(comp);
    }
}
