package ru.innopolis.patterns.builder;

/**
 * Created by innopolis on 25.10.16.
 */
public class Computer {
    private final String motherBoard;
    private final String cpu;
    private final String hdd;
    private final String os;
    private final String DELIMETER = " ";

    private Computer (Builder builder){
        this.motherBoard = builder.motherBoard;
        this.cpu = builder.cpu;
        this.hdd = builder.hdd;
        this.os = builder.os;
    }

    public String getMotherBoard() {
        return motherBoard;
    }


    public String getCpu() {
        return cpu;
    }

    public String getHdd() {
        return hdd;
    }

    public String getOs() {
        return os;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append(DELIMETER)
                .append(motherBoard)
                .append(DELIMETER)
                .append(cpu)
                .append(DELIMETER)
                .append(hdd)
                .append(DELIMETER)
                .append(os)
                .toString();
    }

    public static class Builder {
        private String motherBoard;
        private String cpu;
        private String hdd;
        private String os;

        public Builder withMotherBoard(String motherBoard){
            this.motherBoard = motherBoard;
            return this;
        }

        public Builder withCpu(String cpu){
            this.cpu = cpu;
            return this;
        }

        public Builder withHdd(String hdd){
            this.hdd = hdd;
            return this;
        }

        public Builder withOs(String os){
            this.os = os;
            return this;
        }


        public Computer build(){
            return new Computer(this);
        }
    }
}
