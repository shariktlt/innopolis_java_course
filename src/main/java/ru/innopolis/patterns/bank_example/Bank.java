package ru.innopolis.patterns.bank_example;

import ru.innopolis.patterns.bank_example.credit.*;
import ru.innopolis.patterns.bank_example.deposit.*;
import ru.innopolis.patterns.bank_example.insurance.*;

/**
 * Created by innopolis on 25.10.16.
 */
public class Bank {
    private final String name;
    private final Credit credit;
    private final Deposit deposit;
    private final Insurance insurance;

    private Bank(Builder builder) throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        name = builder.usedBank;
        credit = builder.getCredit();
        deposit = builder.getDeposit();
        insurance = builder.getInsurance();
    }

    public Credit getCredit() {
        return credit;
    }

    public Deposit getDeposit() {
        return deposit;
    }

    public Insurance getInsurance() {
        return insurance;
    }



    public String toString(){
     return new StringBuilder()
                .append("Bank \"")
                .append(name)
                .append("\":\n")
                .append(deposit.getInfo())
                .append("\n")
                .append(credit.getInfo())
                .append("\n")
                .append(insurance.getInfo())
                .toString();
    }

    public static class Builder {
        private Credit credit;
        private Deposit deposit;
        private Insurance insurance;
        private String usedBank;
        private String packageName;
        public Builder() {
            packageName = this.getClass().getPackage().getName().toString()+'.';
        }

        public Bank build() throws IllegalAccessException, ClassNotFoundException, InstantiationException {
            return new Bank(this);
        }


        public Builder useBank(String usedBank){
            String lowered = usedBank.toLowerCase();
            this.usedBank =  lowered.substring(0,1).toUpperCase()+lowered.substring(1);
            return this;
        }
        private String getClassName(String type){
            return new StringBuilder()
                    .append(this.packageName)
                    .append(type.toLowerCase())
                    .append('.')
                    .append(this.usedBank)
                    .append(type)
                    .toString();
        }

        public Credit getCredit() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
            return (Credit) Class.forName(getClassName("Credit")).newInstance();
        }

        public Deposit getDeposit() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
            return (Deposit) Class.forName(getClassName("Deposit")).newInstance();
        }
        public Insurance getInsurance() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
            return (Insurance) Class.forName(getClassName("Insurance")).newInstance();
        }
    }
}
