package ru.innopolis.patterns.bank_example.credit;

/**
 * Created by innopolis on 25.10.16.
 */
public class SberCredit implements Credit {
    private String info = "Sber credit info";
    @Override
    public String getInfo() {
        return info;
    }
}
