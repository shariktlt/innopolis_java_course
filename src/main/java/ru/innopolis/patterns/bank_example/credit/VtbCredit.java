package ru.innopolis.patterns.bank_example.credit;

/**
 * Created by innopolis on 25.10.16.
 */
public class VtbCredit implements Credit {
    private String info = "VTB credit info";
    @Override
    public String getInfo() {
        return info;
    }
}
