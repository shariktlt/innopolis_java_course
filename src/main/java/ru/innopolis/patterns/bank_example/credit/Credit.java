package ru.innopolis.patterns.bank_example.credit;

/**
 * Created by innopolis on 25.10.16.
 */
public interface Credit {
    String getInfo();
}
