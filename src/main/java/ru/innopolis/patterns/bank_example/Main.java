package ru.innopolis.patterns.bank_example;

/**
 * Created by innopolis on 25.10.16.
 */
public class Main {
    public static void main(String[] args) throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        Bank sber = new Bank.Builder().useBank("sber").build();
        Bank vtb =  new Bank.Builder().useBank("vtb").build();
        System.out.println(sber);
        System.out.println(vtb);
        Bank unknown = new Bank.Builder().build();
    }
}
