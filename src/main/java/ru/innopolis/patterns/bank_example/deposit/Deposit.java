package ru.innopolis.patterns.bank_example.deposit;

/**
 * Created by innopolis on 25.10.16.
 */
public interface Deposit {
    String getInfo();
}
