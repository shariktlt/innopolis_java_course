package ru.innopolis.patterns.bank_example.deposit;

/**
 * Created by innopolis on 25.10.16.
 */
public class VtbDeposit implements Deposit {
    private String info = "VTB deposit info";
    @Override
    public String getInfo() {
        return info;
    }
}
