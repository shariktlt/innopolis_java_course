package ru.innopolis.patterns.bank_example.insurance;

/**
 * Created by innopolis on 25.10.16.
 */
public class SberInsurance implements Insurance {
    private String info = "Sber insurance info";
    @Override
    public String getInfo() {
        return info;
    }
}
