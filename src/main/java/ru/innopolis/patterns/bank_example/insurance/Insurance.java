package ru.innopolis.patterns.bank_example.insurance;

/**
 * Created by innopolis on 25.10.16.
 */
public interface Insurance {
    String getInfo();
}
