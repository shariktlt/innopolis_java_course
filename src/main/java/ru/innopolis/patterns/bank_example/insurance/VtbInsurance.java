package ru.innopolis.patterns.bank_example.insurance;

/**
 * Created by innopolis on 25.10.16.
 */
public class VtbInsurance implements Insurance {
    private String info = "VTB insurance info";
    @Override
    public String getInfo() {
        return info;
    }
}
