package ru.innopolis.patterns.deligate;

/**
 * Created by innopolis on 25.10.16.
 */
public class Main {
    public static void main(String[] args) {
        First first = new FirstImpl();
        Second second = new SecondImpl();
        First deligateFirst = new MultiType(first, second);
        Second deligateSecond = (Second) deligateFirst;

        deligateFirst.firstMethod();
        deligateSecond.secondMethod();
    }
}
