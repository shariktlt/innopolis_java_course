package ru.innopolis.patterns.deligate;

/**
 * Created by innopolis on 25.10.16.
 */
public class MultiType implements First, Second {
    private First firstDeligate;
    private Second secondDeligate;

    public MultiType(First first, Second second) {
        this.firstDeligate = first;
        this.secondDeligate = second;
    }

    @Override
    public void firstMethod() {
        firstDeligate.firstMethod();
    }

    @Override
    public void secondMethod() {
        secondDeligate.secondMethod();
    }
}
