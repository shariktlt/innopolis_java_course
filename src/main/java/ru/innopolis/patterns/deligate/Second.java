package ru.innopolis.patterns.deligate;

/**
 * Created by innopolis on 25.10.16.
 */
public interface Second {
    void secondMethod();
}
