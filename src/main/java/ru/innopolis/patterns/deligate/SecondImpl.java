package ru.innopolis.patterns.deligate;

/**
 * Created by innopolis on 25.10.16.
 */
public class SecondImpl implements Second {
    @Override
    public void secondMethod() {
        System.out.println("secondMethod impl");
    }
}
