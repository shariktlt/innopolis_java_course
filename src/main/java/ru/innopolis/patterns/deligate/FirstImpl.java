package ru.innopolis.patterns.deligate;

/**
 * Created by innopolis on 25.10.16.
 */
public class FirstImpl implements First{

    @Override
    public void firstMethod() {
        System.out.println("firstMethod impl");
    }
}
