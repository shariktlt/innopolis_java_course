package ru.innopolis.patterns.prototype;

import java.util.Objects;

/**
 * Created by innopolis on 25.10.16.
 */
public class Part implements Cloneable{
    private String name;
    private int size;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
/*
    public Object myClone() throws CloneNotSupportedException {
        return clone();
    }*/

    @Override
    public String toString() {
        return new StringBuilder()
                .append("Name: ")
                .append(name)
                .append(" Size: ")
                .append(size).toString();
    }
}
