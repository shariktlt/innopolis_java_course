package ru.innopolis.patterns.prototype;

/**
 * Created by innopolis on 25.10.16.
 */
public class Fabric {
    private final Part prototype;

    public Fabric(Part prototype) {
        this.prototype = prototype;
    }

    public Object build() throws CloneNotSupportedException {
        return (Part) prototype.clone();
    }
}
