package ru.innopolis.patterns.prototype;

/**
 * Created by innopolis on 25.10.16.
 */
public class Main {
    public static void main(String[] args) {
        Part part = new Part();
        part.setName("name1");
        part.setSize(1);
        Fabric fabric = new Fabric(part);
        for (int i = 0; i < 5; i++) {
            try {
                System.out.println(fabric.build());
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        }
    }
}
