package ru.innopolis.architecture.model;

/**
 * Created by innopolis on 27.10.16.
 */
public class PassportModel {
    private final int number;
    private final int serialNumber;

    public PassportModel(int number, int serialNumber) {
        this.number = number;
        this.serialNumber = serialNumber;
    }

    public int getNumber() {
        return number;
    }

    public int getSerialNumber() {
        return serialNumber;
    }
}
