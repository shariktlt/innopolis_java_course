package ru.innopolis.architecture.model;

/**
 * Created by innopolis on 27.10.16.
 */
public class StudentModel {
    private final String name;
    private final String lastName;
    private PassportModel passportModel;

    public StudentModel(String name, String lastName, PassportModel passportModel) {
        this.name = name;
        this.lastName = lastName;
        this.passportModel = passportModel;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public PassportModel getPassportModel() {
        return passportModel;
    }
}
