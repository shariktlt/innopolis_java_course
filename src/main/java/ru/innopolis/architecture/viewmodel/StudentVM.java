package ru.innopolis.architecture.viewmodel;

/**
 * Created by innopolis on 27.10.16.
 */
public class StudentVM {
    private final String name;
    private final String lastName;

    public StudentVM(String name, String lastName) {
        this.name = name;
        this.lastName = lastName;
    }
}
