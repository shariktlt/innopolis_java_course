package ru.innopolis.threads;

/**
 * Created by innopolis on 06.10.16.
 */
public class Box {
    private int value;

    public Box(int value) {
        this.value = value;
    }

    /**
     *  Get box value
     * @return
     */
    public int getValue() {
        return value;
    }

    /**
     *  Set box value
     * @param value
     */
    public void setValue(int value) {
        this.value = value;
    }


}
