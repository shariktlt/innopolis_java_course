package ru.innopolis.threads;

/**
 * Created by innopolis on 06.10.16.
 */
public class MyThread extends Thread {
    private final String name;
    private Box counter;

    public MyThread(String name, Box counter) {
        this.name = name;
        this.counter = counter;
        //setDaemon(true);
    }

    @Override
    public void run() {
        /*if("Thread1".equals(this.name)){
            throw new RuntimeException("Die first");
        }*/
        /*int count;
        for(int i =0; i<100000;++i){
            synchronized (this.counter) {
                count = this.counter.getValue();
                this.counter.setValue(++count);
            }
            System.out.println(this.name + " " + i + " counter "+ count);
            *//*try {
                sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*//*
        }*/
        System.out.println("Thread start "+this.name);
        synchronized (counter) {
           // doSome();
            try {
                this.counter.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Thread end " + this.name);
    }
    private void doSome(){
        synchronized (counter){

        }
    }
}
