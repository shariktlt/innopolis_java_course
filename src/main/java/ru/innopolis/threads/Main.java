package ru.innopolis.threads;

import java.util.HashSet;
import java.util.Set;

import static java.lang.Thread.sleep;

/**
 * Created by innopolis on 06.10.16.
 */
public class Main {
    public static void main(String[] args){
        Set<Thread> threadSet = new HashSet<>();
        Box box = new Box(0);
        Thread t1 = new MyThread("Thread1", box);
        Thread t2 = new MyThread("Thread2", box);
        Thread t3 = new MyThread("Thread3", box);
        threadSet.add(t1);
        threadSet.add(t2);
        threadSet.add(t3);
       for(Thread thread: threadSet){
           thread.start();

       }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t1.interrupt();
        synchronized (box) {
            box.notify();
        }
        System.out.println("Main end " + box.getValue());
    }
}
