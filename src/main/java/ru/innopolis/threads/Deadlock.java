package ru.innopolis.threads;

/**
 * Created by innopolis on 06.10.16.
 */
public class Deadlock {
    public static void main(String[] args){
        Object monitor1 = new Object(),
                monitor2 = new Object();
        Thread t1 = new Thread(){
            @Override
            public void run() {
                System.out.println("Thread 1 started");
                synchronized (monitor1){
                    System.out.println("Thread 1 lock monitor 1");
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    synchronized (monitor2){
                        System.out.println("Thread 1 lock monitor 2");
                    }
                }
            }
        };
        Thread t2 = new Thread(){
            @Override
            public void run() {
                System.out.println("Thread 2 started");
                synchronized (monitor2
                        ){
                    System.out.println("Thread 2 lock monitor 2");
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    synchronized (monitor1){
                        System.out.println("Thread 2 lock monitor 1");
                    }
                }
            }
        };
        t1.start();
        t2.start();
    }
}
