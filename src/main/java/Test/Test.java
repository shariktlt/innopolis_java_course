package Test;

import java.io.IOException;

/**
 * Created by innopolis on 04.10.16.
 */
public class Test {
    public static void main(String[] args)  {
        Test test = new Test();
        try {
            test.doSome();
        } catch (IOException e) {
            e.printStackTrace();
            //throw new RuntimeException(e);
        }finally {
            System.out.println("Finally");
        }
        System.out.println("After exception");
    }

    public void doSome() throws IOException {
        int x = 5;
        throw new IOException();
    }
}
